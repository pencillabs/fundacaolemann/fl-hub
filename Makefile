attach:
	docker exec -it fl_wagtail bash 

up:
	docker-compose up

up-dev:
	docker-compose -f dev-docker-compose.yml up

