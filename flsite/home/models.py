from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase


class HomePageCardTag(TaggedItemBase):

    """Tag Class for HomePageCard"""

    content_object = ParentalKey(
        "HomePageCard", related_name="tagged_items", on_delete=models.CASCADE
    )


class HomePage(Page):
    hub_title = RichTextField(blank=True)
    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    content_panels = Page.content_panels + [
        FieldPanel("hub_title", classname="full"),
        ImageChooserPanel("logo"),
    ]

    def get_context(self, request):
        request_tag = request.GET.get("tag")
        tags = HomePageCard.objects.all().values("tags__name")
        context = super().get_context(request)
        context["cards"] = HomePage.get_cards_by_tag(request_tag)
        context["tags"] = HomePage.get_unique_tags(tags)
        return context

    @staticmethod
    def get_cards_by_tag(tag):
        if tag != "all" and tag != None:
            return HomePageCard.objects.filter(tags__name=tag)
        return HomePageCard.objects.all()

    @staticmethod
    def get_unique_tags(tags):
        unique_tags = []
        for tag in tags:
            if not tag in unique_tags:
                unique_tags.append(tag)
        return unique_tags


class HomePageCard(Page):
    tags = ClusterTaggableManager(through=HomePageCardTag, blank=True)
    blocks = StreamField(
        [
            (
                "contents",
                blocks.StructBlock(
                    [
                        (
                            "link",
                            blocks.URLBlock(
                                required=False, form_classname="contents-link"
                            ),
                        ),
                        (
                            "description",
                            blocks.CharBlock(
                                required=False, form_classname="contents-description"
                            ),
                        ),
                    ]
                ),
            ),
        ],
        null=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("blocks", classname="full"),
        FieldPanel("tags"),
    ]
