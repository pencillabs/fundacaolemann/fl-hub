# Visão geral

O hub de serviços da fundação Lemann é um ambiente virtual que irá facilitar o acesso aos diferentes
aplicativos, serviços e outras informações utilizadas pelos times da fundação. Do ponto de vista do
novo colaborador, o objetivo é que o hub seja simples, responsivo e fácil de ser utilizado. Do
ponto de vista do administrador, o objetivo é que seja possível alterar os conteúdos
disponíveis no hub por meio da área administrativa. O hub foi desenvolvido utilizando o framework
CMS [wagtail](https://wagtail.io).


## Visão do usuário

Para o usuário, o portal é bem simples. Ele poderá visualizar todos os serviços, links e demais
conteúdos relevantes para a fundação, e poderá filtra-los por categoria (tudo, colaboração, comunicação).

![relatorio](_static/images/visao-geral-usuario.png)
![relatorio](_static/images/filtros-usuario.png)

## Visão do administrador

Para o administrador, o portal disponibiliza uma área administrativa que pode ser acessada incluindo
ao final da url o texto `/admin` ([http://intra.fundacaolemann.org.br/admin](http://intra.fundacaolemann.org.br/admin)). 
Será necessário informar email e senha para logar. Uma vez logado a seguinte tela será apresentada:

![relatorio](_static/images/wagtail-geral.png)

O wagtail funciona em uma estrutura de árvore. Cada página do site pode ter paginas internas, o que
permite construir uma hierarquia para os conteúdos. Cada página é composta por um ou mais campos, que
são chamados de `fields`. Por ser baseado no framework Django, cada `field` disponível no painel 
administrativo precisa ser desenvolvido no _backend_ do wagtail. Diferente do wordpress, que já traz
muita coisa pronta mas que não dá muita flexibilidade para o desenvolvedor, o wagtail tem a
filosofia contraria. A ideia é que ele seja o mais simples possível do ponto de vista do usuário,
mas que entregue ferramentas sólidas para que desenvolvedores criem sistemas modernos e robustos.

Existe uma página raiz, chamada `Root`. Ela é criada automaticamente pelo wagtail. É dentro dela que
criamos a pagina inicial da central de serviços. Clicando em `Pages`, no menu lateral, é possível
acessar a `Home`.

![relatorio](_static/images/wagtail-pages.png)

Dentro da home teremos os conteúdos que são carregados na pagina inicial da central de serviços.
Cada um destes conteúdos representa um card diferente.

![relatorio](_static/images/wagtail-home.png)

Vamos avaliar o card **Slack**, que aparece na página inicial da central. Esse card é um field
customizado que foi desenvolvido especificamente para a central de serviços. Ele possui um campo de título
e um campo chamado bloco (_blocks_). O field do tipo card pode possui _n_ blocos. Cada bloco representa um
link que irá aparecer dentro do card quando o usuário acessar a página de serviços. Além do campo de
bloco, o card também possui um campo de tags, que podem ser utilizadas para classificar o tipo do
serviço na página inicial. 

![relatorio](_static/images/wagtail-slack.png)

Para adicionar um novo card de serviço na pagina inicial, bastaria fazer
os seguintes passos:

1. acessar a página Home;
2. criar uma página filha, clicando em `add child page`;
3. Selecionar a opção `Home page card`;
4. Dar um nome para o card, incluir o bloco de links e uma tag opcional de classificação.
5. Publicar o novo card;

Para editar um card já existente, basta clicar nele, alterar os `fields` e publica-lo novamente.
O administrador também pode alterar a logo da fundação e o título Central de serviços, tudo pela
área administrativa. Para isso basta editar a página `Home`.

