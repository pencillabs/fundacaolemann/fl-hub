# fl-hub

Para subir o ambiente do portal de serviços, execute o comando

	make up

Isso irá criar o container da aplicação a partir do arquivo `docker-compose.yml`. A aplicação
estará rodando em `http://localhost:8081/`. Para acessar a área administrativa, é necessário criar
o usuário administrador. Acesse o container executando `make attach`, e rode o 
comando `python ./manage.py createsuperuser`. Informe as credenciais e tente logar em `http://localhost:8081/admin`.
Para mais informações, acesse nossa [documentação de usuário](https://pencillabs.gitlab.io/fundacaolemann/fl-hub/).
