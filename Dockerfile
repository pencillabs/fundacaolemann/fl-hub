FROM python:3.7

RUN pip install wagtail
COPY flsite /flsite
WORKDIR flsite
RUN pip install -r requirements.txt
RUN python manage.py migrate
RUN mkdir /tmp/wagtail_db
RUN mv db.sqlite3 /tmp/wagtail_db/
